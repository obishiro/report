@extends('master')
@section('frm-title')
<i class="fa fa-address-book" aria-hidden="true"></i> @lang('ui.kana')
@endsection
@section('tools')
<div class="card-tools">
    <div class="input-group input-group-sm">
     
      <a href="{{ URL::to('addkana')}}" class="btn btn-primary" role="button" aria-pressed="true"><i class="nav-icon fas fa-edit"></i> @lang('ui.btn-add')</a>
     
    </div>
  </div> 
@endsection
@section('content')
<table id="example2" class="table table-bordered table-hover">
    <thead>
    <tr>
    <th>@lang('ui.no')</th>
      <th>@lang('ui.depart_name')</th>
      <th>@lang('ui.book_code')</th>
      <th>@lang('ui.total')</th>
      <th>@lang('ui.date_book')</th>
    </tr>
    </thead>
    <tbody>
       
</table>
@endsection
@section('script')
<!-- DataTables -->
<script src="{{URL::to('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script>
    $(function () {
    
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>
@endsection