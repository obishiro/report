@extends('master')
@section('frm-title')
<i class="fa fa-address-book" aria-hidden="true"></i> @lang('ui.title') {{$army->army_name}}
@endsection
@section('tools')
<div class="card-tools">
    <div class="input-group input-group-sm">
     
      <a href="{{ URL::to('adddepart')}}" class="btn btn-primary" role="button" aria-pressed="true"><i class="nav-icon fas fa-edit"></i> @lang('ui.btn-add')</a>
      &nbsp;&nbsp;
      <button  class="btn btn-danger" role="button" aria-pressed="true"><i class="nav-icon fas fa-trash"></i> @lang('ui.btn-del')</button>
   
    </div>
  </div> 
@endsection
@section('content')

  <!-- /.content-header -->

  <!-- Main content -->

      <!-- Info boxes -->
    
    
    <!-- /.row -->
      

      <div class="row">
        <div class="col-md-12">
          <div class="card ">
            <div class="card-header">
              <h5 class="card-title"> </h5>

              
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="card card-info">
                    <div class="card-header">
                     <h3 class="card-title"><i class="fas fa-calendar-alt"></i> แบบรายงานผลการตรวจเลือกฯ {{$army->army_name}}</h3>  
      
                     
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0">
                     
                   <table   class="table table-head-fixed text-nowrap report">
                     <thead>
                    <tr class="table-success">
                      <td rowspan="5" align="center"  class="align-middle">วันที่</td>
                      <td class="align-middle" align="center" colspan="4" rowspan="3">ยอดขอเรียกฯ ตามแบบ สด.15 </td>
                      <td class="align-middle" align="center" colspan="4"  rowspan="3">ยอดทหารกองเกิน <br>ตามบัญชีเรียกฯ (แบบ สด.16) </td>
                      <td align="center" colspan="23" >จำนวนทหารกองเกินอายุ 21- 29 ปีบริบูรณ์ ที่เข้ารับการตรวจเลือก </td>
                       
                      <td rowspan="5" ><span>สละสิทธิ์ผ่อนผัน</span></td>
                      <td rowspan="5" class="align-middle">หมายเหตุ</td>
                    </tr>
                    <tr class="table-success">
                 
                    
                      <td  align="center" colspan="18">มาเข้ารับการตรวจเลือก </td>
                      <td  align="center" colspan="5" rowspan="2" class="align-middle">ไม่มา หรือมาแล้วแต่ไม่อยู่จนกว่า<br>การตรวจเลือกแล้วเสร็จ </td>
                       
                    
                    </tr>
                    <tr class="table-success">
                    
                      <td align="center" colspan="7">ส่งเข้ากองประจำการ</td>
                      
                      <td align="center" colspan="11" >การตรวจสอบและปล่อยตัว</td>
                       
                      
                    </tr>



                    <tr class="table-success">
                        <td rowspan="2"><span>ทบ.</span></td>
                      <td  rowspan="2"><span>ทร.</span></td>
                      <td rowspan="2"><span>ทอ.</span></td>
                      <td rowspan="2" style="color:#FF0000"><span>รวม</span></td>
                      <td rowspan="2" ><span>อายุ 21 ปี</span></td>
                      <td  rowspan="2" ><span>อายุ 22 - 29 ปี </span></td>
                      <td rowspan="2"><span>คนผ่อนผัน</span></td>
                      <td rowspan="2" style="color:#FF0000"><span>รวม</span></td>
                     
                      <td  align="center" colspan="4" class="align-middle">  
                        ร้องขอเข้ากองประจำการ </td>
                      <td  rowspan="2" ><span>คนหลีกเลี่ยงขัดขืน</span></td>
                      <td  rowspan="2"  ><span>จับสลากแดง</span></td>
                      <td  rowspan="2"  style="color:#FF0000"><span>รวม</span></td>
                      <td rowspan="2"  ><span>คนจำพวกที่ 4</span></td>
                      <td rowspan="2"  ><span>คนจำพวกที่ 3</span></td>
                      <td rowspan="2"  ><span>คนจำพวกที่ 2 (ทั่วไป)</span></td>
                      <td rowspan="2" style="height: 250px"  ><span>คนจำพวกที่ 2 (ภาวะเพศสภาพ)</span></td>
                      <td rowspan="2"  ><span>คนไม่ได้ขนาด</span></td>
                      <td rowspan="2"  ><span>คนขนาดถัดรอง</span></td>
                      <td rowspan="2"  ><span>คนผ่อนผัน ม.29</span></td>
                      <td rowspan="2"  ><span>คนยกเว้น ม.14(1)</span></td>
                      <td rowspan="2"  ><span>ปล่อยเพราะส่งคนร้องขอฯ พอ</span></td>
                      <td rowspan="2"  ><span>จับสลากดำ</span></td>
                      <td rowspan="2"   style="color:#FF0000"><span>รวม</span></td>
                      <td rowspan="2"  ><span>ม.25</span></td>
                      <td rowspan="2"  ><span>ม.27 และคนหลบหนีไปก่อนจับสลาก</span></td>
                      <td rowspan="2"  ><span>ส่งหมายไม่ได้</span></td>
                      <td rowspan="2"  ><span>อื่นๆ</span></td>
                      <td rowspan="2"   style="color:#FF0000"><span>รวม</span></td>
                    
                    </tr>
                    <tr class="table-success">
                      
                      <td><span>ทบ.</span></td>
                      <td><span>ทร.</span></td>
                      <td><span>ทอ.</span></td>
                      <td style="color:#FF0000"><span>รวม</span></td>
                     
                     
                     
                    
                    </tr>
                     </thead>
                     <tbody>
                      @foreach ($datadate as $date_d=>$dtd)
                     <tr class="table-info">
                       <td>{{$dtd->date_name}}</td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                     </tr>
                     @endforeach
                     <tr class="table-success">
                       <td>รวมทั้งสิ้น</td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                     </tr>
                     </tbody>
                   </table>
                  </div>
                  <!-- /.card-body -->
                </div>


                </div>
                <!-- /.col -->
                
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- ./card-body -->
            
            <!-- /.card-footer -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Main row -->
      

@endsection
@section('script')
<!-- DataTables -->
<script src="{{URL::to('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script>
    $(function() {
    var header_height = 0;
    $('table td span').each(function() {
        if ($(this).outerWidth() > header_height) header_height = $(this).outerWidth();
    });

//  $('table td').height(header_height);
});
//Initialize Select2 Elements
$('.select2bs4').select2({
  theme: 'bootstrap4'
});
  </script>
@endsection