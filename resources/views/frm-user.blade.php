@extends('master')
@section('frm-title')
    <i class="fa fa-users"></i> @lang('ui.frm-user')
@endsection
@section('content')
<div class="statusMsg"></div>

  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">รายการผู้ใช้งานระบบ <button id="genpwd" class="btn btn-danger">สร้างรหัสผ่าน</button></h3>
          
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-12">
                <table id="example2" class="table table-bordered table-hover table-striped">
                    <thead class="thead-light">
                    <tr>
                    <th>@lang('ui.no')</th>
                      <th>ยศ ชื่อ-สกุล</th>
                      <th>@lang('ui.army_p')</th>
                        <th>@lang('ui.mtb')</th>
                       
                        <th>@lang('ui.prov')</th>
                        <th>@lang('ui.kana')</th>
                       
                       
                    </tr>
                    </thead>
                    <tbody>
                        <?php $i=1;?>
                   @foreach ($datauser as $users=>$u)
                       
        
                    <tr>
                    <td width="5%" align="center">{{$i}}</td>
                      <td>{{$u->yod_name}} {{$u->user_fname}} {{$u->user_lname}}</td>
                      <td>{{$u->army_shortname}}</td>
                      <td>{{$u->mtb_shortname}}</td>
                    <td>{{$u->PROVINCE_NAME}}</td>
                    <td >{{$u->user_kana}}</td>
                   
                    </tr>
                    <?php $i++;?>
                    @endforeach
                
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                       
                </table>
            </div>
            
           
          </div>
        </div>
        <!-- /.card-body -->
      </div>
    
    </div>
        <!-- left column -->
        
  </div>
  
  

 
<br>
 
@endsection
@section('script')
<!-- DataTables -->
<script src="{{URL::to('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script type="text/javascript">

$("#genpwd").on('click', function(e){
    
          e.preventDefault();
          $.ajax({
              type: 'GET',
              url: '{{URL::to("genpwd")}}',
              //data: new FormData(this),
             // dataType: 'json',
              contentType: false,
              cache: false,
              processData:false,
              beforeSend: function(){
                  $('#genpwd').attr("disabled","disabled");
                  $('#genpwd').css("opacity",".5");
              },
              success: function(response){ //console.log(response);
                  $('.statusMsg').html('');
                  if(response.status == 1){
                      $('#genpwd')[0].reset();
                      $('.statusMsg').html("<div class='alert alert-info alert-dismissible'>"+
                    "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                    "<h5><i class='icon fas fa-check'></i> ผลการทำงาน!</h5>"+response.message+'</div>');
                  }else{
                      $('.statusMsg').html("<div class='alert alert-olive alert-dismissible'>"+
                    "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                    "<h5><i class='icon fas fa-ban'></i> เกิดความผิดพลาด!</h5>"+response.message+'</div>');
                  }
                  $('#genpwd').css("opacity","");
                  $("#genpwd").removeAttr("disabled");
              }
          });
      });
 
    $(function () {
    
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>
@endsection