@extends('master')
@section('frm-title')
<i class="fa fa-address-book" aria-hidden="true"></i> @lang('ui.kana')
@endsection
@section('tools')
<div class="card-tools">
    <div class="input-group input-group-sm">
     
      <a href="{{ URL::to('addkana')}}" class="btn btn-primary" role="button" aria-pressed="true"><i class="nav-icon fas fa-edit"></i> @lang('ui.btn-add')</a>
     
    </div>
  </div> 
@endsection
@section('content')
<table id="example2" class="table table-bordered table-hover table-striped">
    <thead class="thead-dark">
    <tr>
    <th>@lang('ui.no')</th>
      <th>@lang('ui.army_name')</th>
        <th>จำนวน@lang('ui.mtb')</th>
        <th>จำนวน@lang('ui.prov')</th>
        <th>จำนวน@lang('ui.district')</th>
       
       
    </tr>
    </thead>
    <tbody>
        <?php $i=1;?>
    @foreach ($data as $dp=>$d)
        
   
    <tr>
    <td width="5%" align="center">{{$i}}</td>
      <td>{{$d->army_name}}</td>
    <td>{{summtb($d->id)}}</td>
    <td>{{sumprov($d->id)}}</td>
    <td >{{sumkana($d->id)}}</td>
   
    </tr>
  
    <?php 
 
    
    $i++;?>
    @endforeach
    </tbody>
    <tfoot>
      <tr>
      
        <td colspan="2">รวมทั้งสิ้น</td>
      <td>{{number_format($summtb)}}</td>
      <td>{{number_format($sumprov)}}</td>
        <td>{{number_format($sumkana)}}</td>
      </tr>
    </tfoot>
       
</table>
@endsection
@section('script')
<!-- DataTables -->
<script src="{{URL::to('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script>
    $(function () {
    
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>
@endsection