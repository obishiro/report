<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@lang('ui.title')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->

    <link rel="stylesheet" href="{{ URL::to('plugins/all.css')}}">
    <!-- Ionicons -->

  <!-- Ionicons -->

  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ URL::to('plugins/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ URL::to('css/adminlte.css')}}">
  <!-- Google Font: Source Sans Pro -->
 
</head>
 
<body class="hold-transition layout-top-nav" style="background: #f2f2f2">
  <div class="wrapper">
  
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand-md navbar-dark navbar-green">
      <div class="container">
        
    
  
        <div class="collapse navbar-collapse order-3" id="navbarCollapse">
          <!-- Left navbar links -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a href="#" class="nav-link"><b>@lang('ui.title')</b></a>
            </li>
             
          </ul>
  
        
        
      </div>
    </nav>
    <!-- /.navbar -->
    <div class="content-wrapper"  style="background: #f2f2f2">
      <div class="content">
        <div class="container">
<div class="login-box" style="margin-left: 35%;margin-top:5%">
  <div class="login-logo">
    
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
    {{-- <img src="{{URL::to('img/army.png')}}" style="padding-left: 25%;padding-bottom:5%;width:75%"> --}}
    <form action="{{url('post-login')}}" method="POST" id="regForm">
      {{ csrf_field() }}
      <div class="form-group">
        @if ($errors->has('username'))
        <span class="badge badge-danger">  <i class="far fa-times-circle"></i> @lang('ui.pls-input')</span>
  
        @endif
        <input type="text" name="username" class="form-control @if ($errors->has('username')) has-error is-invalid @endif" placeholder="ชื่อผู้ใช้งาน..." @if ($errors->has('username')) id="inputError" @endif>
        <div class="input-group-append">
   
        </div>
      </div>
      <div class="form-group">
        @if ($errors->has('password'))
        <span class="badge badge-danger">  <i class="far fa-times-circle"></i> @lang('ui.pls-input')</span>
  
        @endif
        <input  type="password" name="password"class="form-control @if ($errors->has('password')) has-error is-invalid @endif" placeholder="รหัสผ่าน..." @if ($errors->has('password')) id="inputError" @endif>
        <div class="input-group-append">
   
        </div>
      </div>
      <div class="row">
        <div class="col-8">
          <button type="submit" class="btn btn-primary btn-block"><i class="fas fa-sign-in-alt"></i> เข้าสู่ระบบ</button>
        </div>
        <!-- /.col -->
        <div class="col-4">
          <button type="reset" class="btn btn-danger btn-block"><i class="fas fa-power-off"></i> ยกเลิก</button>
        </div>
        <!-- /.col -->
      </div>
 
       
  </div>
</div>
</div>
    </div>
  </div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ URL::to('plugins/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{ URL::to('plugins/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ URL::to('js/adminlte.js')}}"></script>

</body>
</html>
