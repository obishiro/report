@extends('master')
@section('frm-title')
<i class="fa fa-address-book" aria-hidden="true"></i> @lang('ui.title') {{$army->army_name}}
@endsection
@section('tools')
<div class="card-tools">
    <div class="input-group input-group-sm">
     
      <a href="{{ URL::to('adddepart')}}" class="btn btn-primary" role="button" aria-pressed="true"><i class="nav-icon fas fa-edit"></i> @lang('ui.btn-add')</a>
      &nbsp;&nbsp;
      <button  class="btn btn-danger" role="button" aria-pressed="true"><i class="nav-icon fas fa-trash"></i> @lang('ui.btn-del')</button>
   
    </div>
  </div> 
@endsection
@section('content')

  <!-- /.content-header -->

  <!-- Main content -->

      <!-- Info boxes -->
    
    
    <!-- /.row -->
      

      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
             

              <div class="card-tools">
                <div class="input-group input-group-lg"  >
                  <select name="gendate" id="gendate" class="form-control select2bs4" >
                    <option value="">- เลือกวันที่ -</option>
                    @foreach ($datadate as $date=>$dt)
                    <option value="{{$dt->id}}">{{$dt->date_name}}</option>
                    @endforeach
                </select>

                  <div class="input-group-append">
                    <button type="button" class="btn btn-danger"><i class="fas fa-calendar-alt"></i></button>
                  </div>
                    &nbsp;&nbsp;
                  <div class="input-group-append">
                    <button type="button" class="btn btn-success"><i class="fas fa-print"></i> ส่งออกรายงาน</button>
                  </div>
                  
                </div>
                
                
                
              </div>
            </div>
            <!-- /.card-header -->
           <div id="showdata">
            <div class="card-body">
              <div class="row ">
                <div class="col-md-12">
                  <div class="card card-info">
                    <div class="card-header">
                      <h5 class="card-title"><i class="fas fa-calendar-alt"></i> แบบรายงานผลการตรวจเลือกฯ ประจำวันที่ {{ $datename}}</h5>
          
                     
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0 " >
                     
                      <table   class="table text-nowrap report">
                          <thead>
                         <tr class="table-success">
                           <td rowspan="5" class="align-middle">อำเภอ</td>
                           <td rowspan="5" class="align-middle">จังหวัด/คณะ</td>
                           <td class="align-middle" align="center" colspan="4" rowspan="3">ยอดขอเรียกฯ ตามแบบ สด.15 </td>
                           <td class="align-middle" align="center" colspan="4"  rowspan="3">ยอดทหารกองเกิน <br>ตามบัญชีเรียกฯ (แบบ สด.16) </td>
                           <td align="center" colspan="23" >จำนวนทหารกองเกินอายุ 21- 29 ปีบริบูรณ์ ที่เข้ารับการตรวจเลือก </td>
                            
                           <td rowspan="5" ><span>สละสิทธิ์ผ่อนผัน</span></td>
                           <td rowspan="5" ><span>ปล่อยคนผ่อนผันเสร็จสิ้นเวลา</span></td>
                           <td rowspan="5" ><span>เสร็จสิ้นเวลา</span></td>
                           <td rowspan="5" class="align-middle">หมายเหตุ</td>
                         </tr>
                         <tr class="table-success">
                      
                         
                           <td  align="center" colspan="18">มาเข้ารับการตรวจเลือก </td>
                           <td  align="center" colspan="5" rowspan="2" class="align-middle">ไม่มา หรือมาแล้วแต่ไม่อยู่จนกว่า<br>การตรวจเลือกแล้วเสร็จ </td>
                            
                         
                         </tr>
                         <tr class="table-success">
                         
                           <td align="center" colspan="7">ส่งเข้ากองประจำการ</td>
                           
                           <td align="center" colspan="11" >การตรวจสอบและปล่อยตัว</td>
                            
                           
                         </tr>
                      
                      
                      
                         <tr class="table-success">
                         
                             <td rowspan="2"><span>ทบ.</span></td>
                           <td  rowspan="2"><span>ทร.</span></td>
                           <td rowspan="2"><span>ทอ.</span></td>
                           <td rowspan="2" style="color:#FF0000"><span>รวม</span></td>
                           <td rowspan="2" ><span>อายุ 21 ปี</span></td>
                           <td  rowspan="2" ><span>อายุ 22 - 29 ปี </span></td>
                           <td rowspan="2"><span>คนผ่อนผัน</span></td>
                           <td rowspan="2" style="color:#FF0000"><span>รวม</span></td>
                          
                           <td  align="center" colspan="4" class="align-middle">  
                             ร้องขอเข้ากองประจำการ </td>
                           <td  rowspan="2" ><span>คนหลีกเลี่ยงขัดขืน</span></td>
                           <td  rowspan="2"  ><span>จับสลากแดง</span></td>
                           <td  rowspan="2"  style="color:#FF0000"><span>รวม</span></td>
                           <td rowspan="2"  ><span>คนจำพวกที่ 4</span></td>
                           <td rowspan="2"  ><span>คนจำพวกที่ 3</span></td>
                           <td rowspan="2"  ><span>คนจำพวกที่ 2 (ทั่วไป)</span></td>
                           <td rowspan="2" style="height: 250px"  ><span>คนจำพวกที่ 2 (ภาวะเพศสภาพ)</span></td>
                           <td rowspan="2"  ><span>คนไม่ได้ขนาด</span></td>
                           <td rowspan="2"  ><span>คนขนาดถัดรอง</span></td>
                           <td rowspan="2"  ><span>คนผ่อนผัน ม.29</span></td>
                           <td rowspan="2"  ><span>คนยกเว้น ม.14(1)</span></td>
                           <td rowspan="2"  ><span>ปล่อยเพราะส่งคนร้องขอฯ พอ</span></td>
                           <td rowspan="2"  ><span>จับสลากดำ</span></td>
                           <td rowspan="2"   style="color:#FF0000"><span>รวม</span></td>
                           <td rowspan="2"  ><span>ม.25</span></td>
                           <td rowspan="2"  ><span>ม.27 และคนหลบหนีไปก่อนจับสลาก</span></td>
                           <td rowspan="2"  ><span>ส่งหมายไม่ได้</span></td>
                           <td rowspan="2"  ><span>อื่นๆ</span></td>
                           <td rowspan="2"   style="color:#FF0000"><span>รวม</span></td>
                         
                         </tr>
                         <tr class="table-success">
                          
                           <td><span>ทบ.</span></td>
                           <td><span>ทร.</span></td>
                           <td><span>ทอ.</span></td>
                           <td style="color:#FF0000"><span>รวม</span></td>
                          
                          
                          
                         
                         </tr>
                          </thead>
                          <tbody>
                            @foreach($kana as $datakana=>$k)
                          <tr class="table-info">
                            <td align="center">{{$k->AMPHUR_NAME}} {{checkday($k->gen_day)}}</td>
                            <td align="center">{{$k->Prov_shortname}} {{checkkana($k->kana)}}</td>
                            <td align="center">{{number_format(report_day('army_15',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center">{{number_format(report_day('navy_15',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center">{{number_format(report_day('airforce_15',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center" style="color: #FF0000">{{number_format(sum_data3(report_day('army_15',$k->AMPHUR_ID,$k->gen_date),report_day('navy_15',$k->AMPHUR_ID,$k->gen_date),report_day('airforce_15',$k->AMPHUR_ID,$k->gen_date)))}}</td>
                            <td align="center">{{number_format(report_day('age21',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center">{{number_format(report_day('age_other',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center">{{number_format(report_day('ponpun',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center" style="color: #FF0000">{{number_format(sum_data3(report_day('age21',$k->AMPHUR_ID,$k->gen_date),report_day('age_other',$k->AMPHUR_ID,$k->gen_date),report_day('ponpun',$k->AMPHUR_ID,$k->gen_date)))}}</td>
                            <td align="center">{{number_format(report_day('army_send',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center">{{number_format(report_day('navy_send',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center">{{number_format(report_day('airforce_send',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center" style="color: #FF0000">{{number_format(sum_data3(report_day('army_send',$k->AMPHUR_ID,$k->gen_date),report_day('navy_send',$k->AMPHUR_ID,$k->gen_date),report_day('airforce_send',$k->AMPHUR_ID,$k->gen_date)))}}</td>
                            <td align="center">{{number_format(report_day('leak_send',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center">{{number_format(report_day('red_send',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center" style="color: #FF0000">{{number_format(sum_data3(sum_data3(report_day('army_send',$k->AMPHUR_ID,$k->gen_date),report_day('navy_send',$k->AMPHUR_ID,$k->gen_date),report_day('airforce_send',$k->AMPHUR_ID,$k->gen_date)),report_day('leak_send',$k->AMPHUR_ID,$k->gen_date),report_day('red_send',$k->AMPHUR_ID,$k->gen_date)))}}</td>
                            <td align="center">{{number_format(report_day('p_4',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center">{{number_format(report_day('p_3',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center">{{number_format(report_day('p_2',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center">{{number_format(report_day('p_kratay',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center">{{number_format(report_day('p_kanad',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center">{{number_format(report_day('p_tudrong',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center">{{number_format(report_day('p_29',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center">{{number_format(report_day('p_141',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center">{{number_format(report_day('p_rongpo',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center">{{number_format(report_day('p_black',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center" style="color: #FF0000">{{number_format(sum_data10(
                              report_day('p_4',$k->AMPHUR_ID,$k->gen_date),
                              report_day('p_3',$k->AMPHUR_ID,$k->gen_date),
                              report_day('p_2',$k->AMPHUR_ID,$k->gen_date),
                              report_day('p_kratay',$k->AMPHUR_ID,$k->gen_date),
                              report_day('p_kanad',$k->AMPHUR_ID,$k->gen_date),
                              report_day('p_tudrong',$k->AMPHUR_ID,$k->gen_date),
                              report_day('p_29',$k->AMPHUR_ID,$k->gen_date),
                              report_day('p_141',$k->AMPHUR_ID,$k->gen_date),
                              report_day('p_rongpo',$k->AMPHUR_ID,$k->gen_date),
                              report_day('p_black',$k->AMPHUR_ID,$k->gen_date)
                              ))}}</td>
                            <td align="center">{{number_format(report_day('m25',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center">{{number_format(report_day('m27',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center">{{number_format(report_day('notsend',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center">{{number_format(report_day('not_other',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center" style="color: #FF0000">{{sum_data4(
                              report_day('m25',$k->AMPHUR_ID,$k->gen_date),
                              report_day('m27',$k->AMPHUR_ID,$k->gen_date),
                              report_day('notsend',$k->AMPHUR_ID,$k->gen_date),
                              report_day('not_other',$k->AMPHUR_ID,$k->gen_date)
                            )}}</td>
                            <td align="center">{{number_format(report_day('slasit',$k->AMPHUR_ID,$k->gen_date))}}</td>
                            <td align="center">{{report_day('ploy_time',$k->AMPHUR_ID,$k->gen_date)}}</td>
                            <td align="center">{{report_day('finish_time',$k->AMPHUR_ID,$k->gen_date)}}</td>
                            <td align="center"></td>
                          </tr>
                          @endforeach
                          
                          <tr class="table-success">
                            <td colspan="2" align="center">รวมทั้งสิ้น</td>
                            
                            <td align="center">{{number_format(sum_report_day('army_15',$k->gen_date))}}</td>
                            <td align="center">{{number_format(sum_report_day('navy_15',$k->gen_date))}}</td>
                            <td align="center">{{number_format(sum_report_day('airforce_15',$k->gen_date))}}</td>
                            <td align="center" style="color: #FF0000">{{number_format(sum_data3(sum_report_day('army_15',$k->gen_date),sum_report_day('navy_15',$k->gen_date),sum_report_day('airforce_15',$k->gen_date)))}}</td>
                            <td align="center">{{number_format(sum_report_day('age21',$k->gen_date))}}</td>
                            <td align="center">{{number_format(sum_report_day('age_other',$k->gen_date))}}</td>
                            <td align="center">{{number_format(sum_report_day('ponpun',$k->gen_date))}}</td>
                            <td align="center" style="color: #FF0000">{{number_format(sum_data3(sum_report_day('age21',$k->gen_date),sum_report_day('age_other',$k->gen_date),sum_report_day('ponpun',$k->gen_date)))}}</td>
                            <td align="center">{{number_format(sum_report_day('army_send',$k->gen_date))}}</td>
                            <td align="center">{{number_format(sum_report_day('navy_send',$k->gen_date))}}</td>
                            <td align="center">{{number_format(sum_report_day('airforce_send',$k->gen_date))}}</td>
                            <td align="center" style="color: #FF0000">{{number_format(sum_data3(sum_report_day('army_send',$k->gen_date),sum_report_day('navy_send',$k->gen_date),sum_report_day('airforce_send',$k->gen_date)))}}</td>
                            <td align="center">{{number_format(sum_report_day('leak_send',$k->gen_date))}}</td>
                            <td align="center">{{number_format(sum_report_day('red_send',$k->gen_date))}}</td>
                            <td align="center" style="color: #FF0000">{{number_format(sum_data3(sum_data3(sum_report_day('army_send',$k->gen_date),sum_report_day('navy_send',$k->gen_date),sum_report_day('airforce_send',$k->gen_date)),sum_report_day('leak_send',$k->gen_date),sum_report_day('red_send',$k->gen_date)))}}</td>
                            <td align="center">{{number_format(sum_report_day('p_4',$k->gen_date))}}</td>
                            <td align="center">{{number_format(sum_report_day('p_3',$k->gen_date))}}</td>
                            <td align="center">{{number_format(sum_report_day('p_2',$k->gen_date))}}</td>
                            <td align="center">{{number_format(sum_report_day('p_kratay',$k->gen_date))}}</td>
                            <td align="center">{{number_format(sum_report_day('p_kanad',$k->gen_date))}}</td>
                            <td align="center">{{number_format(sum_report_day('p_tudrong',$k->gen_date))}}</td>
                            <td align="center">{{number_format(sum_report_day('p_29',$k->gen_date))}}</td>
                            <td align="center">{{number_format(sum_report_day('p_141',$k->gen_date))}}</td>
                            <td align="center">{{number_format(sum_report_day('p_rongpo',$k->gen_date))}}</td>
                            <td align="center">{{number_format(sum_report_day('p_black',$k->gen_date))}}</td>
                            <td align="center" style="color: #FF0000">
                              {{number_format(sum_data10(
                                sum_report_day('p_4',$k->gen_date),
                                sum_report_day('p_3',$k->gen_date),
                                sum_report_day('p_2',$k->gen_date),
                                sum_report_day('p_kratay',$k->gen_date),
                                sum_report_day('p_kanad',$k->gen_date),
                                sum_report_day('p_tudrong',$k->gen_date),
                                sum_report_day('p_29',$k->gen_date),
                                sum_report_day('p_141',$k->gen_date),
                                sum_report_day('p_rongpo',$k->gen_date),
                                sum_report_day('p_black',$k->gen_date)
                                ))}}
                            </td>
                            <td align="center">{{number_format(sum_report_day('m25',$k->gen_date))}}</td>
                            <td align="center">{{number_format(sum_report_day('m27',$k->gen_date))}}</td>
                            <td align="center">{{number_format(sum_report_day('notsend',$k->gen_date))}}</td>
                            <td align="center">{{number_format(sum_report_day('not_other',$k->gen_date))}}</td>
                            <td align="center" style="color: #FF0000">
                              {{number_format(sum_data4(
                                sum_report_day('m25',$k->gen_date),
                                sum_report_day('m27',$k->gen_date),
                                sum_report_day('notsend',$k->gen_date),
                                sum_report_day('not_other',$k->gen_date)
                              ))}}
                            </td>
                            <td align="center">{{number_format(sum_report_day('slasit',$k->gen_date))}}</td>
                            <td align="center"></td>
                            <td align="center"></td>
                            <td align="center"></td>
                          </tr>
                          </tbody>
                        </table>
          
              
                  </div>
                  <!-- /.card-body -->
                </div>
          
          
                </div>
                <!-- /.col -->
                
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
          











           </div>
            <!-- ./card-body -->
            
            <!-- /.card-footer -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Main row -->
      
<input type="hidden" id="army" value="{{$army->army_number}}">
@endsection
@section('script')
<!-- DataTables -->
<script src="{{URL::to('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script>
    $(function() {
    var header_height = 0;
    $('table td span').each(function() {
        if ($(this).outerWidth() > header_height) header_height = $(this).outerWidth();
    });

//  $('table td').height(header_height);
});
//Initialize Select2 Elements

 
$('#gendate').change(function(){
          		var StrId=$('#gendate').val();
              var Army =$('#army').val();
              $.ajax({    //create an ajax request to display.php
                type: "GET",
                url: "{{URL::to('getdatareport')}}/"+Army+"/"+StrId,             
                dataType: "html",   //expect html to be returned                
                success: function(response){                    
                    $("#showdata").html(response); 
                    //alert(response);
                }

    });
  });
 
  </script>
@endsection