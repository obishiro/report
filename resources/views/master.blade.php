<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>@lang('ui.title')</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{URL::to('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{URL::to('css/adminlte.css')}}">
  <link rel="stylesheet" href="{{URL::to('css/report.css')}}">
  <!-- Google Font: Source Sans Pro -->
   <!-- Select2 -->
   <link rel="stylesheet" href="{{URL::to('plugins/select2/css/select2.min.css')}}">
   <link rel="stylesheet" href="{{URL::to('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
   <link rel="stylesheet" href="{{URL::to('plugins/daterangepicker/daterangepicker.css')}}">
   <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="{{URL::to('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
   
    <!-- DataTables -->
    <link rel="stylesheet" href="{{URL::to('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{URL::to('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Sarabun:wght@200;500&display=swap" rel="stylesheet">
</head>
<body class="hold-transition layout-top-nav   layout-fixed layout-navbar-fixed layout-footer-fixed">
  <div class="wrapper">

    
    <nav class="main-header navbar navbar-expand navbar-green navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav" >
        
        <li class="nav-item d-none d-sm-inline-block">
          <a href="{{URL::to('reportall')}}" class="nav-link"  style="color: #FFF">แบบรายงานยอดรวม</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
          <a href="{{URL::to('report')}}" class="nav-link"  style="color: #FFF">แบบรายงานยอดประจำวัน</a>
        </li>
         <li class="nav-item d-none d-sm-inline-block">
          <a href="{{URL::to('frm-basic')}}" class="nav-link" style="color: #FFF">บันทึกวันตรวจเลือกฯ</a>
        </li>
        
        <li class="nav-item d-none d-sm-inline-block">
          <a href="{{URL::to('frm-total')}}" class="nav-link"  style="color: #FFF">@lang('ui.frmsave43')</a>
        </li>
        
        @if(Auth::user()->user_type==1)
        <li class="nav-item d-none d-sm-inline-block">
          <a href="{{URL::to('users')}}" class="nav-link"  style="color: #FFF">ข้อมูลผู้ใช้งาน</a>
        </li>
        @endif
        
        <li class="nav-item d-none d-sm-inline-block">
          <a href="{{URL::to('exit')}}" class="nav-link"  style="color: #FFF">ออกจากระบบ</a>
        </li>
      </ul>
  
  
      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
    
        <!-- Notifications Dropdown Menu -->
        
      
      </ul>
    </nav>
    <!-- /.navbar -->
  
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-12">
              <h1 class="m-0 text-dark">@yield('frm-title')</h1>
            </div><!-- /.col -->
            <!-- <div class="col-sm-4">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
    
                <li class="breadcrumb-item active">@lang('ui.title')</li>
              </ol>
            </div>/.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      
    <!-- Content Wrapper. Contains page content -->
    <section class="content">
      <div class="container-fluid">
        @yield('content')
      <!-- /.row -->
    </div><!--/. container-fluid -->
  </section>
  <!-- /.content -->
</div>
    <!-- /.content-wrapper -->
  
    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  
    <!-- Main Footer -->
    <footer class="main-footer">
      <strong>พัฒนาโดย &copy; แผนกสัสดี กองการสัสดี หน่วยบัญชาการรักษาดินแดน </strong>
      
      
    </footer>
  </div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{URL::to('plugins/jquery.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{URL::to('plugins/bootstrap.bundle.js')}}"></script>

<script src="{{URL::to('js/adminlte.js')}}"></script>
<!-- Select2 -->
<script src="{{URL::to('plugins/select2/js/select2.full.min.js')}}"></script>
<!-- InputMask -->
<script src="{{URL::to('plugins/moment/moment.min.js')}}"></script>
<script src="{{URL::to('plugins/inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{URL::to('plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- date-range-picker -->

@yield('script')
</body>
</html>
