@extends('master')
@section('frm-title')
    <i class="fa fa-edit"></i>@lang('ui.frmsave43')
@endsection
@section('content')

<form role="form"  id="fupForm">
  {{ csrf_field() }}
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <div class="card card-gray">
        <div class="card-header">
          <h3 class="card-title">ข้อมูลคณะกรรมการตรวจเลือกฯ</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-3">
              <div class="form-group">
                <label >วันที่ทำการตรวจเลือกฯ</label>
                <select name="gendate" id="" class="form-control select2bs4" required >
                    <option value="">- เลือกวันที่ -</option>
                    @foreach ($datadate as $date=>$dt)
                    <option value="{{$dt->id}}">{{$dt->date_name}}</option>
                    @endforeach
                </select>
              </div>
            </div>
            <div class="col-3">
              <div class="form-group">
                <label >จังหวัดที่ทำการตรวจเลือกฯ</label>
                <select name="Prov" id="Prov" class="form-control select2bs4" required >
                  <option value="">- เลือกจังหวัด -</option>
                  @foreach ($dataamphur as $amphur=>$amp)
                  <option value="{{$amp->PROVINCE_ID}}">{{$amp->PROVINCE_NAME}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-3">
              <div class="form-group">
                <label >คณะที่</label>
                <select name="Kana" id="Kana" class="form-control select2bs4" required >
                    <option value="">- เลือกคณะที่ -</option>
                    <option value="0"> คณะเดียว</option>
                    @for ($i=1;$i<=8;$i++)
                    <option value="{{$i}}">คณะที่ {{$i}}</option>
                    @endfor
                </select>
              </div>
            </div>
            <div class="col-3">
              <div class="form-group">
                <label >อำเภอทำการตรวจเลือกฯ</label>
                <select name="District" id="District" class="form-control select2bs4" required >
                  
                </select>
              </div>
            </div>
           
          </div>
        </div>
        <!-- /.card-body -->
      </div>
    
    </div>

    <div class="col-md-6">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">ยอดขอเรียกก่อนการตรวจเลือก</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-4">
              <input type="text" name="txt-army_15" class="form-control numberic" required placeholder="ยอด ทบ.">
            </div>
            <div class="col-4">
              <input type="text" name="txt-navy_15" class="form-control numberic" required placeholder="ยอด ทร.">
            </div>
            <div class="col-4">
              <input type="text" name="txt-airforce_15" class="form-control numberic" required placeholder="ยอด ทอ.">
            </div>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
    
    </div>
        <!-- left column -->
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">ตามบัญชีเรียกฯ (แบบ สด.16)</h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-4">
                  <input type="text" name="txt-age21" class="form-control numberic" required placeholder="อายุ 21 ปี">
                </div>
                <div class="col-4">
                  <input type="text" name="txt-age_other" class="form-control numberic" required placeholder="อายุ 22 - 29 ปี">
                </div>
                <div class="col-4">
                  <input type="text" name="txt-ponpun" class="form-control numberic" required placeholder="คนผ่อนผัน">
                </div>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
        
        </div>
  </div>
  <div class="row">
    <!-- left column -->
    <div class="col-md-6">
      <div class="card card-orange">
        <div class="card-header">
          <h3 class="card-title">ส่งเข้ากองประจำการ</h3>
        </div>
        <div class="card-body">
          <div class="row">
          <div class="col-md-6">
         
            <div class="card card-lightblue">
              <div class="card-header">
                <h3 class="card-title">ร้องขอเข้ากองประจำการ</h3>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-4">
                    <input type="text" name="txt-army_send" class="form-control numberic" required placeholder="ยอด ทบ.">
                  </div>
                  <div class="col-4">
                    <input type="text" name="txt-navy_send" class="form-control numberic" required placeholder="ยอด ทร.">
                  </div>
                  <div class="col-4">
                    <input type="text" name="txt-airforce_send" class="form-control numberic" required placeholder="ยอด ทอ.">
                  </div>
                   
                </div>
              </div>
              <!-- /.card-body -->
            </div>
          
          </div>
          <div class="col-md-6">
            <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">คนหลีกเลี่ยงฯ และจับสลากแดง</h3>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-6">
                    <input type="text" name="txt-leak_send" class="form-control numberic" required placeholder="คนหลีกเลี่ยงฯ">
                  </div>
                  <div class="col-6">
                    <input type="text" name="txt-red_send" class="form-control numberic" required placeholder="จับสลากแดง">
                  </div>
                  
                </div>
              </div>
              <!-- /.card-body -->
            </div>
          
          </div>
        </div>

        </div>
        <!-- /.card-body -->
      </div>
    
    </div>
        <!-- left column -->
        <div class="col-md-6">
          <div class="card card-success">
            <div class="card-header">
              <h3 class="card-title">การตรวจสอบและปล่อยตัว</h3>
            </div>
            <div class="card-body">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-3">
                    <input type="text" name="txt-p_4" class="form-control numberic" required placeholder="จำพวกที่ 4">
                  </div>
                  <div class="col-3">
                    <input type="text" name="txt-p_3" class="form-control numberic" required placeholder="จำพวกที่ 3">
                  </div>
                  <div class="col-3">
                    <input type="text" name="txt-p_2" class="form-control numberic" required placeholder="จำพวกที่ 2">
                  </div>
                  <div class="col-3">
                    <input type="text" name="txt-p_kratay" class="form-control numberic" required placeholder="เพศสภาพฯ">
                  </div>
                   
                </div>
                <br>
                  <div class="row">
                  <div class="col-3">
                    <input type="text" name="txt-p_kanad" class="form-control numberic" required placeholder="คนไม่ได้ขนาด">
                  </div>
                  <div class="col-3">
                    <input type="text" name="txt-p_tudrong" class="form-control numberic" required placeholder="คนขนาดถัดรอง">
                  </div>
                  <div class="col-3">
                    <input type="text" name="txt-p_293" class="form-control numberic" required placeholder="คนผ่อนผัน ม.29">
                  </div>
                  <div class="col-3">
                    <input type="text" name="txt-p_141" class="form-control numberic" required placeholder="ยกเว้น ม.14">
                  </div>
                   
                </div>
                <br>
                <div class="row">
                <div class="col-6">
                  <input type="text" name="txt-p_rongpo" class="form-control numberic" required placeholder="ปล่อยเพราะส่งคนร้องขอพอ">
                </div>
                <div class="col-6">
                  <input type="text" name="txt-p_black" class="form-control numberic" required placeholder="จับสลากดำ">
                </div>
                 
                 
              </div>
                
                

              </div>
              
              
            </div>
            <!-- /.card-body -->
          </div>
        
        </div>
        
  </div>
  <div class="row">
    <!-- left column -->
    <div class="col-md-6">
      <div class="card card-maroon">
        <div class="card-header">
          <h3 class="card-title">ไม่เข้าตรวจเลือกฯ</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-3">
              <input type="text" name="txt-m25" class="form-control numberic" required placeholder="ม.25">
            </div>
            <div class="col-3">
              <input type="text" name="txt-m27" class="form-control numberic" required placeholder="ม.27และหลบหนีฯ">
            </div>
            <div class="col-3">
              <input type="text" name="txt-notsend" class="form-control numberic" required placeholder="ส่งหมายไม่ได้">
            </div>
            <div class="col-3">
              <input type="text" name="txt-not_other" class="form-control numberic" required placeholder="อื่นๆ">
            </div>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
    
    </div>
        <!-- left column -->
        <div class="col-md-6">
          <div class="card card-gray">
            <div class="card-header">
              <h3 class="card-title">สละสิทธิ์ผ่อนผัน และเวลาตรวจเลือกฯ</h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-4">
                  <input type="text" name="txt-slasit" class="form-control numberic" required placeholder="สละสิทธิ์ผ่อนผัน">
                </div>
                <div class="col-4">
                  <input type="text" name="txt-ploy_time" class="form-control" required placeholder="ปล่อยคนผ่อนผันเวลา">
                </div>
                <div class="col-4">
                  <input type="text" name="txt-finish_time" class="form-control" required placeholder="เสร็จสิ้นเวลา">
                </div>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
        
        </div>
  </div>

 <div class="card-footer">
   <div class="row">
     <div class="col-6 statusMsg"></div>
   </div>
   <div class="row">
   <div class="col-4">
  <button type="submit" class="btn btn-primary btn-lg col-12">@lang('ui.btn-save')</button>
</div>
  <div class="col-2">
  <button type="reset" class="btn btn-danger btn-lg col-12">@lang('ui.btn-cancle')</button>
</div>
</div>
</div>
<br>
</form>
@endsection
@section('script')
<script type="text/javascript">

$(document).ready(function(e){
    $.ajaxSetup({
      beforeSend: function(xhr, type) {
          if (!type.crossDomain) {
              xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
          }
      },
  });
  $('.numberic').keyup(function(e)
                                {
  if (/\D/g.test(this.value))
  {
    // Filter non-digits from input value.
    this.value = this.value.replace(/\D/g, '');
  }
});
      // Submit form data via Ajax
      $("#fupForm").on('submit', function(e){
          e.preventDefault();
          $.ajax({
              type: 'POST',
              url: '{{URL::to("adddata")}}',
              data: new FormData(this),
              dataType: 'json',
              contentType: false,
              cache: false,
              processData:false,
              beforeSend: function(){
                  // $('.submitBtn').attr("disabled","disabled");
                  // $('#fupForm').css("opacity",".5");
              },
              success: function(response){ //console.log(response);
                  $('.statusMsg').html('');
                  if(response.status == 1){
                      $('#fupForm')[0].reset();
                      $('.statusMsg').html("<div class='alert alert-info alert-dismissible'>"+
                    "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                    "<h5><i class='icon fas fa-check'></i> ผลการทำงาน!</h5>"+response.message+'</div>');
                  }else{
                      $('.statusMsg').html("<div class='alert alert-olive alert-dismissible'>"+
                    "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                    "<h5><i class='icon fas fa-ban'></i> เกิดความผิดพลาด!</h5>"+response.message+'</div>');
                  }
                  $('.statusMsg').show();
                  $('.statusMsg').hide('2000');
              }
          });
      });
  });
 
 

  $('#Kana').change(function(){
              var StrId=$('#Prov').val();
              var kana=$('#Kana').val();
      
              $.ajax({    //create an ajax request to display.php
                type: "GET",
                url: "{{URL::to('check/prov')}}/"+StrId+"/"+kana,             
                dataType: "html",   //expect html to be returned                
                success: function(response){                    
                    $("#District").html(response); 
                    //alert(response);
                }

    });
  });
  

  //  		  $('#District').change(function(){
  //         		var StrId=$('#District').val();
  //             $.ajax({    //create an ajax request to display.php
  //               type: "GET",
  //               url: "{{URL::to('check/amphur')}}/"+StrId,             
  //               dataType: "html",   //expect html to be returned                
  //               success: function(response){                    
  //                   $("#Tumbon").html(response); 
  //                   //alert(response);
  //               }

  //   });
  // });
  // $('#Depart').change(function(){
  //         		var StrId=$('#Depart').val();
  //             $.ajax({    //create an ajax request to display.php
  //               type: "GET",
  //               url: "{{URL::to('check/depart')}}/"+StrId,             
  //               dataType: "html",   //expect html to be returned                
  //               success: function(response){                    
  //                   $("#Bookcode").val(response); 
  //                   //alert(response);
  //               }

  //   });
  // });


//Initialize Select2 Elements
$('.select2bs4').select2({
  theme: 'bootstrap4'
});


 
 
 
  
    
  </script>
 @endsection