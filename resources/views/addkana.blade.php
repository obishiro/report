@extends('master')
@section('frm-title')
    <i class="fa fa-university"></i> @lang('ui.depart_name')
@endsection
 
@section('content')
<form role="form" enctype="multipart/form-data"   id="fupForm">
    {{ csrf_field() }}
  <div class="row"> 
      <div class="col-md-6">
        <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title"><i class="fa fa-building"></i> @lang('ui.depart_name')</h3>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-sm-12">
          <div class="form-group">
            <label for="">ประเภท @lang('ui.depart_name')</label>
      
              <select name="txt-depart_type_name"   id="Departtype"  class="form-control select2bs4" style="width: 100%;" required>
                <option value="">- เลือกข้อมูล -</option>
                @foreach($geo as $go=>$g)
              <option value="{{$g->GEO_ID}}">{{$g->GEO_NAME}}</option>
                @endforeach
              </select>
          </div>
          </div>
        </div>
          
          <div class="row">
              <div class="col-sm-12">
              <div class="form-group">
                <label for="">@lang('ui.depart_name')</label>
          
                  <input name="txt-depart_name"   id=""  class="form-control" required>
                    
              </div>
              </div>
            </div>
          <div class="row">
              <div class="col-sm-12">
                <!-- text input -->
                <div class="form-group">
                  <label>@lang('ui.book_code')</label>
                  <input type="text" name="txt-book_code" class="form-control"  placeholder="" required>
                </div>
              </div>
                           
            </div>
            <div class="row">
                <div class="col-sm-12">
                  <!-- text input -->
                  <div class="form-group">
                    <label>@lang('ui.depart_address')</label>
                    <textarea name="txt-depart_address" class="form-control"  placeholder="" required></textarea>
                  </div>
                </div>
                             
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="statusMsg"></div>
                </div>
              </div>
              <div class="row">

                <div class="col-sm-12">
                  <button class="btn btn-success submitBtn" type="submit" id=""><i class="fa fa-save"></i> @lang('ui.btn-save')</button>
                  <button class="btn btn-danger" type="button" id=""><i class="fa fa-power-off"></i> @lang('ui.btn-cancle')</button>
                </div>
              </div>
      </div>
        </div>
      </div>
             
      </div>
</form>
@endsection
@section('script')
<!-- DataTables -->
<script src="{{URL::to('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::to('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script>
    $(function () {
    
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>
  @section('script')
 
  <script>
  $(document).ready(function(e){
    $.ajaxSetup({
      beforeSend: function(xhr, type) {
          if (!type.crossDomain) {
              xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
          }
      },
  });
      // Submit form data via Ajax
      $("#fupForm").on('submit', function(e){
          e.preventDefault();
          $.ajax({
              type: 'POST',
              url: '{{URL::to("adddepart")}}',
              data: new FormData(this),
              dataType: 'json',
              contentType: false,
              cache: false,
              processData:false,
              beforeSend: function(){
                  $('.submitBtn').attr("disabled","disabled");
                  $('#fupForm').css("opacity",".5");
              },
              success: function(response){ //console.log(response);
                  $('.statusMsg').html('');
                  if(response.status == 1){
                      $('#fupForm')[0].reset();
                      $('.statusMsg').html("<div class='alert alert-info alert-dismissible'>"+
                    "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                    "<h5><i class='icon fas fa-check'></i> ผลการทำงาน!</h5>"+response.message+'</div>');
                  }else{
                      $('.statusMsg').html("<div class='alert alert-danger alert-dismissible'>"+
                    "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                    "<h5><i class='icon fas fa-ban'></i> เกิดความผิดพลาด!</h5>"+response.message+'</div>');
                  }
                  $('#fupForm').css("opacity","");
                  $(".submitBtn").removeAttr("disabled");
              }
          });
      });
  });
 
    //Initialize Select2 Elements
$('.select2bs4').select2({
  theme: 'bootstrap4'
});
  </script>
   
   
      
  @endsection
@endsection