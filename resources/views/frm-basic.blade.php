@extends('master')
@section('frm-title')
    <i class="fa fa-edit"></i> บันทึกข้อมูลอำเภอที่ตรวจเลือกฯ
@endsection
@section('content')

<form role="form"  id="fupForm">
  {{ csrf_field() }}
  <div class="row">
    <!-- left column -->
    <div class="col-md-4">
        <div class="card card-success">
          <div class="card-header">
            <h3 class="card-title">สถานที่ตรวจเลือกฯ</h3>
          </div>
          <div class="card-body">
            <div class="form-group">
                <label >วันที่ทำการตรวจเลือกฯ</label>
                <select name="txt-date" id="txt-date" class="form-control select2bs4">
                    <option value="">- เลือกวันที่ -</option>
                    @foreach ($datadate as $date=>$dt)
                    <option value="{{$dt->id}}">{{$dt->date_name}}</option>
                    @endforeach
                </select>
              </div>

              <div class="form-group">
                <label >กองทัพภาค</label>
                <select name="Army" id="Army" class="form-control select2bs4">
                    <option value="">- เลือกกองทัพภาค -</option>
                    @foreach ($army as $arm=>$amp)
                    <option value="{{$amp->army_number}}">{{$amp->army_name}}</option>
                    @endforeach
                </select>
              </div>
              <div class="form-group">
                <label >จังหวัดที่ทำการตรวจเลือกฯ</label>
                <select name="Prov" id="Prov" class="form-control select2bs4">
                   
                </select>
              </div>
              
          </div>
          <!-- /.card-body -->
        </div>
      
      </div>
    <div class="col-md-4">
      <div class="card card-danger">
        <div class="card-header">
          <h3 class="card-title">ข้อมูลอำเภอ</h3>
        </div>
        <div class="card-body">
          <div class="form-group">
            <label >คณะที่</label>
            <select name="Kana" id="Kana" class="form-control select2bs4">
                <option value="">- เลือกคณะที่ -</option>
                <option value="0"> คณะเดียว</option>
                @for ($i=1;$i<=8;$i++)
                <option value="{{$i}}">คณะที่ {{$i}}</option>
                @endfor
            </select>
          </div>
             
              <div class="form-group">
                <label >อำเภอทำการตรวจเลือกฯ</label>
                <select name="District" id="District" class="form-control select2bs4">
                     
                </select>
              </div>
              <div class="form-group">
                <label >วันที่</label>
                <select name="genday" id="genday" class="form-control select2bs4">
                    <option value="">- เลือกวันที่ -</option>
                    <option value="0">วันเดียว</option>
                    @for ($ii=1;$ii<=12;$ii++)
                    <option value="{{$ii}}">วันที่ {{$ii}}</option>
                    @endfor
                </select>
              </div>
              
             
          </div>
        <!-- /.card-body -->
      </div>
    
    </div>
       
  </div>
  
  

 <div class="card-footer">
  <div class="row">
    <div class="col-6 statusMsg"></div>
  </div>
   <div class="row">
   <div class="col-4">
  <button type="submit" class="btn btn-primary btn-lg col-12">@lang('ui.btn-save')</button>
</div>
  <div class="col-2">
  <input type="reset" class="btn btn-danger btn-lg col-12" value="@lang('ui.btn-cancle')">
</div>
</div>
</div>
<br>
</form>
@endsection
@section('script')
<script type="text/javascript">

$(document).ready(function(e){
    $.ajaxSetup({
      beforeSend: function(xhr, type) {
          if (!type.crossDomain) {
              xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
          }
      },
  });
  
      // Submit form data via Ajax
      $("#fupForm").on('submit', function(e){
          e.preventDefault();
          $.ajax({
              type: 'POST',
              url: '{{URL::to("adddatakana")}}',
              data: new FormData(this),
              dataType: 'json',
              contentType: false,
              cache: false,
              processData:false,
              beforeSend: function(){
                 
              },
              success: function(response){ //console.log(response);
                  $('.statusMsg').html('');
                  if(response.status == 1){
                     // $('#fupForm')[0].reset();
                      $('.statusMsg').html("<div class='alert alert-info alert-dismissible'>"+
                    "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                    "<h5><i class='icon fas fa-check'></i> ผลการทำงาน!</h5>"+response.message+'</div>');
                  }else{
                      $('.statusMsg').html("<div class='alert alert-olive alert-dismissible'>"+
                    "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                    "<h5><i class='icon fas fa-ban'></i> เกิดความผิดพลาด!</h5>"+response.message+'</div>');
                  }
                  $('.statusMsg').show();
                  $('.statusMsg').hide('2000');
                //  $('#fupForm')[0].reset();
                  
              }
          });
      });
  });
 
 
 
 


//Initialize Select2 Elements
$('.select2bs4').select2({
  theme: 'bootstrap4'
});

 
$('#Army').change(function(){
          		var StrId=$('#Army').val();
              $.ajax({    //create an ajax request to display.php
                type: "GET",
                url: "{{URL::to('getdata/army')}}/"+StrId,             
                dataType: "html",   //expect html to be returned                
                success: function(response){                    
                    $("#Prov").html(response); 
                    //alert(response);
                }

    });
  });
 
 
 
// $('#Prov').change(function(){
//           		var StrId=$('#Prov').val();
//               $.ajax({    //create an ajax request to display.php
//                 type: "GET",
//                 url: "{{URL::to('getdata/prov')}}/"+StrId,             
//                 dataType: "html",   //expect html to be returned                
//                 success: function(response){                    
//                     $("#District").html(response); 
//                     //alert(response);
//                 }

//     });
//   });
$('#Kana').change(function(){
              var StrId=$('#Prov').val();
              var kana=$('#Kana').val();
      
              $.ajax({    //create an ajax request to display.php
                type: "GET",
                url: "{{URL::to('check/prov')}}/"+StrId+"/"+kana,             
                dataType: "html",   //expect html to be returned                
                success: function(response){                    
                    $("#District").html(response); 
                    //alert(response);
                }

    });
  });
  
 
  
    
  </script>
 @endsection