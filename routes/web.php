<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});
Route::post('post-login','BackendController@postLogin');
Route::get('reportall','BackendController@getReportall');
Route::get('report','BackendController@getReport');

Route::get('frm-total','BackendController@getFrmtotal');
Route::get('frm-basic','BackendController@getFrmbasic');


Route::get('users','BackendController@getUsers');
Route::get('genpwd','BackendController@getGenpwd');


Route::get('army','BackendController@getArmy');
Route::get('editarmy/{id}','BackendController@getEditarmy')->where(array('id'=>'[0-9]+'));




Route::get('kana','BackendController@getKana');
Route::get('addkana','BackendController@getAddkana');



Route::get('depart','BackendController@getDepart');
Route::get('adddepart','BackendController@getAdddepart');
Route::post('adddepart','BackendController@postAdddepart');


Route::post('adddata','BackendController@postAdddata');
Route::post('adddatakana','BackendController@postAdddatakana');
Route::get('frm','BackendController@getForm');
Route::get('getdata/{type}/{id}','BackendController@getGetdata')->where(array('type' =>'[A-Za-z]+','id'=>'[0-9]+'));
Route::get('check/{type}/{id}/{kana}','BackendController@getCheck')->where(array('type' =>'[A-Za-z]+','id'=>'[0-9]+','kana'=>'[0-9]+'));
Route::get('report/depart/{type}/{id}','BackendController@getReportdepart')->where(array('type' =>'[0-9]+','id'=>'[0-9]+'));
Route::get('export','BackendController@getExport');
Route::get('exit','BackendController@getExit');


// getdata report
Route::get('getdatareport/{army}/{gendate}','BackendController@getDataReport')->where(array('army' =>'[0-9]+','gendate'=>'[0-9]+'));
