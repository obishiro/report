<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kana extends Model
{
    //
    protected $table = 'tb_kana';
    public $timestamps = false;
}
