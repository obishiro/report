<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mtb extends Model
{
    //
    protected $table = 'tb_mtb';
    public $timestamps = false;
}
