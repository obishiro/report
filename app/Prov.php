<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prov extends Model
{
    //
    protected $table = 'province';
    public $timestamps = false;
}
