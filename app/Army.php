<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Army extends Model
{
    //
    protected $table = 'tb_army';
    public $timestamps = false;
}
