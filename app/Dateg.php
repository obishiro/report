<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dateg extends Model
{
    //
    protected $table = 'tb_date';
    public $timestamps = false;
}
