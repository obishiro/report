<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator,Redirect,Response;
Use App\User;
use App\Prov;
use App\Amphur;
use App\Tumbon;
use App\Depart;
use App\Sd13;
use App\Kana;
use App\Departtype;
use App\Army;
use App\Mtb;
use App\Dateg;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;

class BackendController extends Controller
{
    public function postLogin(Request $request)
    {
        request()->validate([
        'username' => 'required',
        'password' => 'required',
        ]);
 
        $credentials = $request->only('username', 'password');
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            
            return redirect()->intended('reportall');
           //Auth::user()->user_fname;
        }
        return Redirect::to("/")->withSuccess('Oppes! You have entered invalid credentials');
    }
    public function getReportall()
    {
        
        //,DB::raw('count(*) as user_count')
        $date = Dateg::all();
        $gen_date = 1;

        if(Auth::check())
        {
            $army = Army::where('army_number',Auth::user()->user_army)->first();
            return view('report_army',['datadate'=>$date,'army'=>$army,'gen_date'=>$gen_date]);
             }else{
            return redirect('/');
    }
    }
    public function getReport()  //รายงานข้อมูลประจำวัน
    {
        
        
        if(Auth::check())
        {
           // $gen_date = 1;
           // $d = date('Y-m-d');
            $d = '2021-04-01';
        $date = Dateg::all();

        $getdate = Dateg::where('date_format',$d)->first();
        $gen_date = $getdate->id;
        $datename = $getdate->date_name;
        $kana = DB::table('tb_kana')
        ->select('tb_kana.gen_day','tb_kana.gen_date','tb_kana.kana','province.Prov_shortname','amphur.AMPHUR_NAME','amphur.AMPHUR_ID')
        ->join('province','tb_kana.prov','=','province.PROVINCE_ID')
        ->join('amphur','tb_kana.amphur','=','amphur.AMPHUR_ID')
        ->where('tb_kana.gen_date',$gen_date)->get();
            $army = Army::where('army_number',Auth::user()->user_army)->first();
       return view('report_day',['datadate'=>$date,'army'=>$army,'gen_date'=>$gen_date,'kana'=>$kana,'datename'=>$datename]);
             }else{
        return redirect('/');
    }
    }

// form

public function getFrmtotal()
{
    
    //,DB::raw('count(*) as user_count')

    if(Auth::check())
    {
        $sqlarmy = Army::all();
        $amphur = Prov::where('GEO_ID',Auth::user()->user_army)
      //  ->where('kana',Auth::user()->user_kana)
         ->orderBy('PROVINCE_NAME','asc')
         ->get();
         
     $date = Dateg::all();

        return view('frm-total',['dataarmy'=>$sqlarmy,'dataamphur'=>$amphur,'datadate'=>$date]);
    }else{
    return redirect('/');
}
}

public function getFrmbasic()
{
    
    //,DB::raw('count(*) as user_count')

    if(Auth::check())
    {
        // $amphur = Prov::where('GEO_ID',Auth::user()->user_army)
        //    //->where('kana',Auth::user()->user_army)
        //     ->orderBy('PROVINCE_NAME','asc')
        //     ->get();
        
        $date = Dateg::all();
        $sqlarmy = Army::all();
        return view('frm-basic',['army'=>$sqlarmy,'datadate'=>$date]);
    }else{
    return redirect('/');
}
}

    public function getArmy()
    {
        if(Auth::check())
        {
        $data = Army::all();
            $sumprov = Prov::count();
            $sumkana = Amphur::count();
            $summtb = Mtb::count();
          return view('army',['data'=>$data,'sumprov'=>$sumprov,'sumkana'=>$sumkana,'summtb'=>$summtb]);
        }else{
        return redirect('/');
    }
    }
    public function getKana()
    {
        if(Auth::check())
        {
        //,DB::raw('count(*) as user_count')
       return view('kana');
    }else{
        return redirect('/');
    }
    }
    public function getAddkana()
    {
        if(Auth::check())
        {
            $geo = Geo::all();
        return view('addkana',['army'=>$army]);
        }else{
            return redirect('/');
        }
    }


    public function getDepart()
    {
        $depart = Depart::orderBy('depart_name','asc')->get();
        return view('depart',['depart'=>$depart]);
    }
    public function getAdddepart()
    {
        if(Auth::check())
        {
            $dep = Departtype::all();
        return view('adddepart',['departtype'=>$dep]);
        }else{
            return redirect('/');
        }
    }
    public function postAdddepart(Request $request)

    {
        if(Auth::check())
        {
        // if($u){
                 
        //     $response = array( 
        //         'status' => 1, 
        //         'message' => "บันทึกข้อมูบเรียบร้อยแล้ว"
        //     ); 
        // }else{
        //     $response = array( 
        //         'status' => 0, 
        //         'message' => 'ไม่สามารถบันทึกข้อมูลได้'
        //     ); 
        // }
        $depart = new Depart;
        $depart->depart_type = $request->input('txt-depart_type_name');
        $depart->depart_name = $request->input('txt-depart_name');
        $depart->depart_code = $request->input('txt-book_code');
        $depart->depart_address = $request->input('txt-depart_address');
        if($depart->save())
        {
        $response = array( 
            'status' => 1, 
            'message' => "บันทึกข้อมูบเรียบร้อยแล้ว"
        ); 
    }else{
        $response = array( 
            'status' => 0, 
            'message' => 'ไม่สามารถบันทึกข้อมูลได้'
        ); 
      
        
    }
      return json_encode($response);
}else{
    return  redirect('/');
}
    }

public function getUsers()
{
    if(Auth::check())
    {
    $prov = Prov::orderBy('PROVINCE_NAME','asc')->get();
    $army = Army::orderBy('army_name','asc')->get();
    $mtb = MTB::orderBy('mtb_name','asc')->get();
    $datauser = User::select('users.user_yod','users.user_fname','users.user_lname','users.user_kana','tb_army.army_shortname','tb_mtb.mtb_shortname','province.PROVINCE_NAME')
    ->join('tb_army','users.user_army','tb_army.army_number')
    ->join('tb_mtb','users.user_mtb','tb_mtb.id')
    ->join('province','users.user_prov','province.PROVINCE_ID')
    ->join('tb_yod','users.user_yod','tb_yod.id')
    ->where('users.user_type','2')
    ->orderBy('tb_army.army_number','asc')
    ->get();
    return view('frm-user',['prov'=>$prov,'army'=>$army,'mtb'=>$mtb,'datauser'=>$datauser]);
    }else{
        return redirect('/');
    }
}

public function getGenpwd()
{
    if(Auth::check())
    {
        $sql= User::where('password','0')
        ->where('user_type','2')->get();
        
            foreach($sql as $data => $d)
            {
                $newpwd = Hash::make($d->username);
                $update = DB::table('users')
                         ->where('password','0')
                         ->where('user_type','2')
                         ->where('username',$d->username)
                        ->update(['password' => $newpwd]);
            }
     
        $response = array( 
            'status' => 1, 
            'message' => "บันทึกข้อมูบเรียบร้อยแล้ว"
        ); 
            
        
   
      return json_encode($response);
     
    }else{
        return redirect('/');
    }
}
 
    public function getForm()
    {
        if(Auth::check())
        {
        $prov = Prov::orderBy('PROVINCE_NAME','asc')->get();
        $depart = Depart::orderBy('depart_name','asc')->get();
        $pol = Pol::orderBy('pol_name','asc')->get();
        return view('form',['prov'=>$prov,'depart'=>$depart,'pol'=>$pol]);
        }else{
            return redirect('/');
        }
    }


    public function postAdddata(Request $request)
    {
        if (Auth::check())
        {
        
         $p = new Sd13;
       

         
         $p->army_15 =$request->input('txt-army_15');

         $p->navy_15 =$request->input('txt-navy_15');

         $p->airforce_15 =$request->input('txt-airforce_15');

         $p->age21 =$request->input('txt-age21');

         $p->age_other = $request->input('txt-age_other');

         $p->ponpun = $request->input('txt-ponpun');

         $p->army_send = $request->input('txt-army_send');

         $p->navy_send = $request->input('txt-navy_send');

         $p->airforce_send = $request->input('txt-airforce_send');

         $p->leak_send = $request->input('txt-leak_send');

         $p->red_send = $request->input('txt-red_send');

         $p->p_4 = $request->input('txt-p_4');

         $p->p_3 = $request->input('txt-p_3');

         $p->p_2 = $request->input('txt-p_2');

         $p->p_kratay = $request->input('txt-p_kratay');

         $p->p_kanad = $request->input('txt-p_kanad');

         $p->p_tudrong = $request->input('txt-p_tudrong');

         $p->p_29 = $request->input('txt-p_293');

         $p->p_141 = $request->input('txt-p_141');

         $p->p_rongpo = $request->input('txt-p_rongpo');

         $p->p_black = $request->input('txt-p_black');

         $p->m25 = $request->input('txt-m25');

         $p->m27 = $request->input('txt-m27');

         $p->notsend = $request->input('txt-notsend');
         $p->not_other = $request->input('txt-not_other');

         $p->slasit = $request->input('txt-slasit');

         $p->ploy_time = $request->input('txt-ploy_time');

         $p->finish_time = $request->input('txt-finish_time');
         $p->gendate = $request->input('gendate');
         $p->province = $request->input('Prov');
         $p->amphur=$request->input('District');
         $p->kana=$request->input('Kana');
         $p->created_at=date('Y-m-d H:i:s');
         $p->updated_at=date('Y-m-d H:i:s');


    

         if($p->save())
         {
         $response = array( 
             'status' => 1, 
             'message' => "บันทึกข้อมูลเรียบร้อยแล้ว"
         ); 
            }else{
         $response = array( 
             'status' => 0, 
             'message' => 'ไม่สามารถบันทึกข้อมูลได้'
         ); 
       
         
     }
       return json_encode($response);

    }else{
        return redirect('/');
    }

    }
    public function postAdddatakana(Request $request)
    {
        if (Auth::check())
        {
            $p = new Kana;
            $p->army_number = $request->input('Army');
            $p->gen_date = $request->input('txt-date');
            $p->prov = $request->input('Prov');
            $p->amphur = $request->input('District');
            $p->kana= $request->input('Kana');
            $p->gen_day= $request->input('genday');

            
         if($p->save())
         {
         $response = array( 
             'status' => 1, 
             'message' => "บันทึกข้อมูลเรียบร้อยแล้ว"
         ); 
            }else{
         $response = array( 
             'status' => 0, 
             'message' => 'ไม่สามารถบันทึกข้อมูลได้'
         ); 
       
         
     }
       return json_encode($response);

    }else{
        return redirect('/');
    }

    }
        
    public function getCheck($type,$id,$kana )
    {
        if(Auth::check())
        {
        switch($type):
            case 'prov':
                $p = Amphur::where('PROVINCE_ID',$id)->where('kana',$kana)->orderBy('AMPHUR_NAME','asc')->get();
                    echo "<option value=''>- เลือกข้อมูล -</option>";
                    foreach($p as $pp=>$a)
                    {
                    echo "<option value='".$a->AMPHUR_ID."'>".$a->AMPHUR_NAME."</option> ";
                    }
                     
                    
            break;
            case 'amphur':
                $p = Tumbon::where('AMPHUR_ID',$id)->orderBy('DISTRICT_NAME','asc')->get();
                    echo "<option value=''>- เลือกข้อมูล -</option>";
                    foreach($p as $pp=>$a)
                    {
                    echo "<option value='".$a->DISTRICT_CODE."'>".$a->DISTRICT_NAME."</option> ";
                    }
                     
                    
            break;
            case 'depart':
                 $Depart = Depart::find($id);
                 return $Depart->depart_code;
            break;
        endswitch;
    }else{
        return redirect('/');
    }
    }

    public function getGetdata($type,$id)
    {
        if(Auth::check())
        {
        switch($type):
            case 'army':
                $p = Prov::where('GEO_ID',$id)->orderBy('PROVINCE_NAME','asc')->get();
                    echo "<option value=''>- เลือกข้อมูล -</option>";
                    foreach($p as $pp=>$a)
                    {
                    echo "<option value='".$a->PROVINCE_ID."'>".$a->PROVINCE_NAME."</option> ";
                    }
                     
                    
            break;
            case 'prov':
                $p = Amphur::where('PROVINCE_ID',$id)->orderBy('AMPHUR_NAME','asc')->get();
                    echo "<option value=''>- เลือกข้อมูล -</option>";
                    foreach($p as $pp=>$a)
                    {
                    echo "<option value='".$a->AMPHUR_ID."'>".$a->AMPHUR_NAME."</option> ";
                    }
                     
                    
            break;
            case 'amphur':
                $p = Tumbon::where('AMPHUR_ID',$id)->orderBy('DISTRICT_NAME','asc')->get();
                    echo "<option value=''>- เลือกข้อมูล -</option>";
                    foreach($p as $pp=>$a)
                    {
                    echo "<option value='".$a->DISTRICT_CODE."'>".$a->DISTRICT_NAME."</option> ";
                    }
                     
                    
            break;
            case 'depart':
                 $Depart = Depart::find($id);
                 return $Depart->depart_code;
            break;
        endswitch;
    }else{
        return redirect('/');
    }
    }
    public function getReportdepart($did,$id)
    {
        
        $sd43 = Sd43::where(array(
            'depart_id' =>$did,
            'depart_number'=>$id

        ))->get();
        $depart = Depart::find($did);
        
        //,DB::raw('count(*) as user_count')
       return view('reportdepart',['sd43'=>$sd43,'dp'=>$depart,'number'=>$id]);
        
    }
    public function getExport()
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('exp_template/first.docx'));

    $title = 'ทดสอบ';
    $reg = 'รายงาน';

    $templateProcessor->setValue('title', $title);
    $templateProcessor->setValue('num', '๑๑');
 
   
   

    $fileName = $title;
    $templateProcessor->saveAs($fileName . '.docx');
    return response()->download($fileName . '.docx')->deleteFileAfterSend(true);
       
    }
   public function getExit()
    {
        Auth::logout();
        return redirect('/');
    }

    public function getDataReport($army,$gendate)  //ดึงรายงานข้อมูล
    {  
       // $gen_date = 1;
        
        //,DB::raw('count(*) as user_count')
        $date = Dateg::all();
        $datename = Dateg::find($gendate);
        $kana = DB::table('tb_kana')
        ->select('tb_kana.gen_day','tb_kana.gen_date','tb_kana.kana','province.Prov_shortname','amphur.AMPHUR_NAME','amphur.AMPHUR_ID')
        ->join('province','tb_kana.prov','=','province.PROVINCE_ID')
        ->join('amphur','tb_kana.amphur','=','amphur.AMPHUR_ID')
        ->where('tb_kana.gen_date',$gendate)->get();
            $army = Army::where('army_number',$army)->first();
       return view('data_report',['datadate'=>$date,'army'=>$army,'gen_date'=>$gendate,'kana'=>$kana,'datename'=>$datename]);
    }
}